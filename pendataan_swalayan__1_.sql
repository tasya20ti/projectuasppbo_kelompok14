-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2021 at 02:19 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pendataan_swalayan`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` varchar(11) NOT NULL,
  `nama_barang` varchar(225) NOT NULL,
  `harga_barang` varchar(20) NOT NULL,
  `jumlah_barang` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `harga_barang`, `jumlah_barang`) VALUES
('B01', 'Indomie', '3000', '22'),
('B02', 'Teh Botol', '5000', '60'),
('B03', 'Milo', '7000', '20'),
('B04', 'Ovaltine', '5000', '30'),
('B05', 'Kapal Api', '8000', '20'),
('B06', 'Dettol', '12000', '50'),
('B07', 'Mamy Poko', '35000', '25'),
('B08', 'Daia', '22000', '30'),
('B09', 'Soklin', '5000', '25'),
('B10', 'Laurier', '25000', '40');

-- --------------------------------------------------------

--
-- Table structure for table `nota_pemasukan`
--

CREATE TABLE `nota_pemasukan` (
  `kode_pemasukan` char(6) NOT NULL,
  `id_barang` varchar(11) NOT NULL,
  `nama_barang` varchar(225) NOT NULL,
  `banyak_barang` varchar(15) NOT NULL,
  `harga_barang` varchar(20) NOT NULL,
  `jumlah_harga` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nota_pemasukan`
--

INSERT INTO `nota_pemasukan` (`kode_pemasukan`, `id_barang`, `nama_barang`, `banyak_barang`, `harga_barang`, `jumlah_harga`) VALUES
('KM01', 'B01', 'Indomie', '2', '3000', '6000'),
('KM02', 'B02', 'Teh Botol', '10', '5000', '50000');

-- --------------------------------------------------------

--
-- Table structure for table `nota_pengeluaran`
--

CREATE TABLE `nota_pengeluaran` (
  `kode_pengeluaran` char(6) NOT NULL,
  `id_barang` varchar(11) NOT NULL,
  `nama_barang` varchar(225) NOT NULL,
  `banyak_barang` varchar(11) NOT NULL,
  `harga_barang` varchar(20) NOT NULL,
  `jumlah_harga` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nota_pengeluaran`
--

INSERT INTO `nota_pengeluaran` (`kode_pengeluaran`, `id_barang`, `nama_barang`, `banyak_barang`, `harga_barang`, `jumlah_harga`) VALUES
('KN01', 'B04', 'Ovaltine', '5', '5000', '25000'),
('KN02', 'B02', 'Teh Botol', '5', '5000', '25000'),
('KN03', 'B03', 'Milo', '5', '7000', '35000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `nota_pemasukan`
--
ALTER TABLE `nota_pemasukan`
  ADD PRIMARY KEY (`kode_pemasukan`),
  ADD KEY `notapemasukan_barang` (`id_barang`);

--
-- Indexes for table `nota_pengeluaran`
--
ALTER TABLE `nota_pengeluaran`
  ADD PRIMARY KEY (`kode_pengeluaran`),
  ADD KEY `notapengeluaran_barang` (`id_barang`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nota_pemasukan`
--
ALTER TABLE `nota_pemasukan`
  ADD CONSTRAINT `notapemasukan_barang` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `nota_pengeluaran`
--
ALTER TABLE `nota_pengeluaran`
  ADD CONSTRAINT `notapengeluaran_barang` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
