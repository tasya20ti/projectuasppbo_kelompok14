package View;

import View.*;
import Controller.c_barang;
import Controller.c_notapemasukan;
import Controller.c_notapengeluaran;
import Model.m_barang;
import Model.m_notapemasukan;
import Model.m_notapengeluaran;

public class Data extends javax.swing.JFrame {
    
    public Data() {
        initComponents();
        setVisible(true);
        setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        notapemasukan = new javax.swing.JButton();
        notapengeluaran = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cetak = new javax.swing.JButton();
        keluar = new javax.swing.JButton();
        data = new javax.swing.JButton();
        Dashboard = new javax.swing.JButton();
        input = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(225, 95, 65));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(255, 190, 118));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        notapemasukan.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        notapemasukan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/notapemasukan.png"))); // NOI18N
        notapemasukan.setText("Nota Pemasukan");
        notapemasukan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                notapemasukanActionPerformed(evt);
            }
        });

        notapengeluaran.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        notapengeluaran.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/pengeluaran.png"))); // NOI18N
        notapengeluaran.setText("Nota Pengeluaran");
        notapengeluaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                notapengeluaranActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(notapengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(notapemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(67, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(notapemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(notapengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52))
        );

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/12.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(284, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(353, 353, 353))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(271, 271, 271))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addComponent(jLabel5)
                .addGap(26, 26, 26)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(117, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 0, 910, 830));

        jPanel1.setBackground(new java.awt.Color(250, 152, 58));

        jLabel1.setFont(new java.awt.Font("Forte", 1, 24)); // NOI18N
        jLabel1.setText("YumNanTa");

        cetak.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cetak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/cetak2.png"))); // NOI18N
        cetak.setText("CETAK");
        cetak.setHideActionText(true);
        cetak.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        cetak.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        cetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cetakActionPerformed(evt);
            }
        });

        keluar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        keluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/keluar2.png"))); // NOI18N
        keluar.setText("KELUAR");
        keluar.setHideActionText(true);
        keluar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        keluar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keluarActionPerformed(evt);
            }
        });

        data.setBackground(new java.awt.Color(255, 190, 118));
        data.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        data.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/data2.png"))); // NOI18N
        data.setText("DATA");
        data.setHideActionText(true);
        data.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        data.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        data.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dataActionPerformed(evt);
            }
        });

        Dashboard.setBackground(new java.awt.Color(255, 255, 255));
        Dashboard.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Dashboard.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/rumah.png"))); // NOI18N
        Dashboard.setText("DASHBOARD");
        Dashboard.setHideActionText(true);
        Dashboard.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Dashboard.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        Dashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DashboardActionPerformed(evt);
            }
        });

        input.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        input.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/input (2).png"))); // NOI18N
        input.setText("INPUT");
        input.setHideActionText(true);
        input.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        input.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        input.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cetak, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(keluar, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(data, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(Dashboard, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50)))
                .addContainerGap(29, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(Dashboard, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(input, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(data, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                .addComponent(cetak, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(keluar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 290, 830));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void notapengeluaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_notapengeluaranActionPerformed
        // TODO add your handling code here:
        m_notapengeluaran data = new m_notapengeluaran();
        NotaPengeluaran frm = new NotaPengeluaran();
        c_notapengeluaran ctrl = new c_notapengeluaran(data,frm) {};
        ctrl.Tampildata();
        ctrl.id_barang();
        frm.setVisible(true);
        dispose();
    }//GEN-LAST:event_notapengeluaranActionPerformed

    private void notapemasukanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_notapemasukanActionPerformed
        // TODO add your handling code here:
        m_notapemasukan data = new m_notapemasukan();
        NotaPemasukan frm = new NotaPemasukan();
        c_notapemasukan ctrl = new c_notapemasukan(data,frm) {};
        ctrl.KosongForm();
        ctrl.Tampildata();
        ctrl.id_barang();
        frm.setVisible(true);
        dispose();
    }//GEN-LAST:event_notapemasukanActionPerformed

    private void cetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cetakActionPerformed
        // TODO add your handling code here:
        Cetak in = new Cetak();
        in.setVisible(true);
        dispose();
    }//GEN-LAST:event_cetakActionPerformed

    private void keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keluarActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_keluarActionPerformed

    private void dataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dataActionPerformed
        // TODO add your handling code here:
        Data da = new Data();
        da.setVisible(true);
        dispose();
    }//GEN-LAST:event_dataActionPerformed

    private void DashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DashboardActionPerformed
        // TODO add your handling code here:
        Dashboard das = new Dashboard();
        das.setVisible(true);
        dispose();
    }//GEN-LAST:event_DashboardActionPerformed

    private void inputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputActionPerformed
        // TODO add your handling code here:
        Input in = new Input();
        in.setVisible(true);
        dispose();
    }//GEN-LAST:event_inputActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Data.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Data.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Data.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Data.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Data().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton Dashboard;
    public javax.swing.JButton cetak;
    public javax.swing.JButton data;
    public javax.swing.JButton input;
    public javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel5;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel2;
    public javax.swing.JPanel jPanel3;
    public javax.swing.JButton keluar;
    public javax.swing.JButton notapemasukan;
    public javax.swing.JButton notapengeluaran;
    // End of variables declaration//GEN-END:variables

}
