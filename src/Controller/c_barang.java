package Controller;
import Model.ConnectorPendataanSwalayan;
import Model.m_barang;
import View.Barang;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class c_barang implements ActionListener, MouseListener{  
    private m_barang data;
    private Barang frm;

    public c_barang(m_barang data, Barang frm) {
        this.data = data;
        this.frm = frm;
        this.frm.tambah.addActionListener(this);

        this.frm.edit.addActionListener(this);
        this.frm.hapus.addActionListener(this);
        this.frm.tabel_barang.addMouseListener(this);
    }
    
    public void KosongForm(){
        frm.Txtidbarang.setEditable(true);
        frm.Txtidbarang.setText(null);
        frm.Txtnamabarang.setText(null);
        frm.Txthargabarang.setText(null);
        frm.Txtjumlahbarang.setText(null);
        
    }
    
    public void Tampildata(){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("No");
        model.addColumn("Id Barang");
        model.addColumn("Nama Barang");
        model.addColumn("Harga Barang");
        model.addColumn("Jumlah Barang");
        
        // menampilkan data pada database ke dalam tabel
        try{
            int no = 1;
            String sql = "SELECT * FROM barang";
            java.sql.Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            
            while(res.next()){
                model.addRow(new Object[] {
                    no++,
                    res.getString(1),
                    res.getString(2),
                    res.getString(3),
                    res.getString(4),});
                }
                frm.tabel_barang.setModel(model);
                
            }
            catch(SQLException e){
            System.out.println("Error "+e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae){
        if(ae.getSource() == frm.tambah){
            KosongForm();
        }
//        else if(ae.getSource() == frm.simpan){
//            data.setId_barang(frm.Txtidbarang.getText());
//            data.setNama_barang(frm.Txtnamabarang.getText());
//            data.setHarga_barang(frm.Txthargabarang.getText());
//            data.setJumlah_barang(frm.Txtjumlahbarang.getText());
//           
//            try {
//                if(data.SimpanInfoStokBarang(data)){
//                    JOptionPane.showMessageDialog(null, "Simpan Data Baru Berhasil");
//                    KosongForm();
//                    Tampildata();
//                }
//            }
//            catch(SQLException ex){
//                JOptionPane.showMessageDialog(null, ex);
//            }
//        }
        
        else if(ae.getSource() == frm.edit){
            data.setId_barang(frm.Txtidbarang.getText());
            data.setNama_barang(frm.Txtnamabarang.getText());
            data.setHarga_barang(frm.Txthargabarang.getText());
            data.setJumlah_barang(frm.Txtjumlahbarang.getText());
            
            try{
                if(data.UpdateInfoStokBarang(data)){
                    JOptionPane.showMessageDialog(null, "Data berhasil diupdate!");
                    KosongForm();
                    Tampildata();
                }
            }
            catch(SQLException ex){
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        
        else{
            data.setId_barang(frm.Txtidbarang.getText());
            
            try{
                if(data.HapusInfoStokBarang(data)){
                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus!");
                    KosongForm();
                    Tampildata();
                }
            }
            catch(SQLException  ex){
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent me){
        if(me.getSource() == frm.tabel_barang){
            frm.Txtidbarang.setEditable(false);
            
            int baris = frm.tabel_barang.rowAtPoint(me.getPoint());
            String id_barang = frm.tabel_barang.getValueAt(baris, 1).toString();
            frm.Txtidbarang.setText(id_barang);
            String nama_barang = frm.tabel_barang.getValueAt(baris, 2).toString();
            frm.Txtnamabarang.setText(nama_barang);
            String harga_barang = frm.tabel_barang.getValueAt(baris, 3).toString();
            frm.Txthargabarang.setText(harga_barang);
            String jumlah_barang = frm.tabel_barang.getValueAt(baris, 4).toString();
            frm.Txtjumlahbarang.setText(jumlah_barang);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
