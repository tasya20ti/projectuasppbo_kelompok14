package Controller;
import Model.ConnectorPendataanSwalayan;
import Model.m_notapemasukan;
import View.NotaPemasukan;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class c_notapemasukan implements ActionListener, MouseListener{
    private m_notapemasukan data;
    private NotaPemasukan frm;

    public c_notapemasukan(m_notapemasukan data, NotaPemasukan frm){
        this.data = data;
        this.frm = frm;
        this.frm.tambah.addActionListener(this);
        this.frm.simpan.addActionListener(this);
//      this.frm.edit.addActionListener(this);
        this.frm.hapus.addActionListener(this);
        this.frm.tabel_notapemasukan.addMouseListener(this);
    }
    
    public void KosongForm(){
        frm.Txtkodepemasukan.setEditable(true);
        frm.Txtkodepemasukan.setText(null);
        frm.Txtidbarang.setSelectedItem(null);
        frm.Txtnamabarang.setEditable(false);
        frm.Txtnamabarang.setText(null);
        frm.Txtbanyakbarang.setText(null);
        frm.Txthargabarang.setEditable(false);
        frm.Txthargabarang.setText(null);
        frm.Txtjumlahharga.setText(null);
        frm.Txtjumlahharga.setEditable(false);
    }
    
    public void Tampildata(){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("No");
        model.addColumn("Kode Pemasukan");
        model.addColumn("Id Barang");
        model.addColumn("Nama Barang");
        model.addColumn("Banyak Barang");
        model.addColumn("Harga Barang");
        model.addColumn("Jumlah Harga");
        
        // menampilkan data pada database ke dalam tabel
        try{
            int no = 1;
            String sql = "SELECT * FROM nota_pemasukan";
            java.sql.Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            
            while(res.next()){
                model.addRow(new Object[] {
                    no++,
                    res.getString(1),
                    res.getString(2),
                    res.getString(3),
                    res.getString(4),
                    res.getString(5),
                    res.getString(6),});
                }
                frm.tabel_notapemasukan.setModel(model);
                
            }
            catch(SQLException e){
            System.out.println("Error "+e.getMessage());
        }
    }

//    public void KurangStok(){
//        try{
//            int jumlah = Integer.parseInt(frm.Txtbanyakbarang.getText());
//            int jumlah_lama = 0;
//            String sql = "SELECT jumlah_barang FROM barang WHERE id_barang = " +frm.Txtidbarang.getSelectedItem();
//            java.sql.Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
//            java.sql.Statement stm = conn.createStatement();
//            java.sql.ResultSet res = stm.executeQuery(sql);
//
//            if(res.next()){
//                jumlah_lama = Integer.parseInt(res.getString("jumlah_barang"));
//                int jumlah_baru = jumlah_lama - jumlah;
//                String stokbaru = jumlah_baru+"";
//                String sql2 = "UPDATE barang SET jumlah_barang = ? WHERE id_barang = ?";
//                PreparedStatement pst = conn.prepareStatement(sql2);
//                pst.setString(1, stokbaru);
//                pst.setString(2, (String) frm.Txtidbarang.getSelectedItem());
//                pst.execute();
//            }
//        }
//        catch(SQLException e){
//            System.out.println("Error "+e.getMessage());
//        }
//    }
    
    public void id_barang(){
        try{
            String sql = "SELECT * FROM barang";
            Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery(sql);
            
            while(res.next()){
                frm.Txtidbarang.addItem(res.getString(1));
            }
            
            res.last();
            int data = res.getRow();
            res.first();
        }
        catch(SQLException e){
            System.out.println("Error "+e.getMessage());
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent ae){
        if(ae.getSource() == frm.tambah){
            KosongForm();
        }
        else if(ae.getSource() == frm.simpan){
            data.setKode_pemasukan(frm.Txtkodepemasukan.getText());
            data.setId_barang((String) frm.Txtidbarang.getSelectedItem());
            data.setNama_barang(frm.Txtnamabarang.getText());
            data.setBanyak_barang(frm.Txtbanyakbarang.getText());
            data.setHarga_barang(frm.Txthargabarang.getText());
            int banyakbarang = Integer.parseInt(frm.Txtbanyakbarang.getText());
            int hargabarang = Integer.parseInt(frm.Txthargabarang.getText());
            int jumlah_harga = banyakbarang * hargabarang;
            frm.Txtjumlahharga.setText("" + jumlah_harga);
            data.setJumlah_harga(frm.Txtjumlahharga.getText());
            
            try {
                if(data.SimpanNotaPemasukan(data)){
                    JOptionPane.showMessageDialog(null, "Data berhasil disimpan!");
//                    KurangStok();
                    KosongForm();
                    Tampildata();
                }
            }
            catch(SQLException ex){
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        
//        else if(ae.getSource() == frm.edit){
//            data.setKode_pengeluaran(frm.Txtkodepengeluaran.getText());
//            data.setId_barang((String) frm.Txtidbarang.getSelectedItem());
//            data.setNama_barang(frm.Txtnamabarang.getText());
//            data.setBanyak_barang(frm.Txtbanyakbarang.getText());
//            data.setHarga_barang(frm.Txthargabarang.getText());
//            int banyakbarang = Integer.parseInt(frm.Txtbanyakbarang.getText());
//            int hargabarang = Integer.parseInt(frm.Txthargabarang.getText());
//            int jumlah_harga = banyakbarang * hargabarang;
//            frm.Txtjumlahharga.setText("" + jumlah_harga);
//            data.setJumlah_harga(frm.Txtjumlahharga.getText());
//            
//            try{
//                if(data.UpdateNotaPengeluaran(data)){
//                    JOptionPane.showMessageDialog(null, "Update Data Baru Berhasil");
//                    KurangStok();
//                    KosongForm();
//                    Tampildata();
//                }
//            }
//            catch(SQLException ex){
//                JOptionPane.showMessageDialog(null, ex);
//            }
//        }
//        
        else{
            data.setKode_pemasukan(frm.Txtkodepemasukan.getText());
            
            try{
                if(data.HapusNotaPemasukan(data)){
                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus!");
                    KosongForm();
                    Tampildata();
                }
            }
            catch(SQLException  ex){
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent me){
        if(me.getSource() == frm.tabel_notapemasukan){
            frm.Txtkodepemasukan.setEditable(false);
            
            int baris = frm.tabel_notapemasukan.rowAtPoint(me.getPoint());
            String kode_pemasukan = frm.tabel_notapemasukan.getValueAt(baris, 1).toString();
            frm.Txtkodepemasukan.setText(kode_pemasukan);
            String id_barang = frm.tabel_notapemasukan.getValueAt(baris, 2).toString();
            frm.Txtidbarang.setSelectedItem(id_barang);
            String nama_barang = frm.tabel_notapemasukan.getValueAt(baris, 3).toString();
            frm.Txtnamabarang.setText(nama_barang);
            String banyak_barang = frm.tabel_notapemasukan.getValueAt(baris, 4).toString();
            frm.Txtbanyakbarang.setText(banyak_barang);
            String harga_barang = frm.tabel_notapemasukan.getValueAt(baris, 5).toString();
            frm.Txthargabarang.setText(harga_barang);
            String jumlah_harga = frm.tabel_notapemasukan.getValueAt(baris, 6).toString();
            frm.Txtjumlahharga.setText(jumlah_harga);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
