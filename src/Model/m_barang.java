package Model;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class m_barang extends ConnectorPendataanSwalayan{
    private String id_barang;
    private String nama_barang;
    private String harga_barang;
    private String jumlah_barang;

    public String getId_barang() {
        return id_barang;
    }

    public void setId_barang(String id_barang) {
        this.id_barang = id_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(String harga_barang) {
        this.harga_barang = harga_barang;
    }

    public String getJumlah_barang() {
        return jumlah_barang;
    }

    public void setJumlah_barang(String jumlah_barang) {
        this.jumlah_barang = jumlah_barang;
    }
    
    public boolean SimpanInfoStokBarang(m_barang data) throws SQLException{
        PreparedStatement pstm = null;
        Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
        
        String sql = "INSERT INTO barang(id_barang, nama_barang, harga_barang, jumlah_barang) VALUES (?,?,?,?)";
        
        try{
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, data.getId_barang());
            pstm.setString(2, data.getNama_barang());
            pstm.setString(3, data.getHarga_barang());
            pstm.setString(4, data.getJumlah_barang());
            pstm.execute();
            return true;
        }
        catch(HeadlessException | SQLException e){
            System.err.println(e);
            return false;
        }
    }
    
    public boolean UpdateInfoStokBarang(m_barang data) throws SQLException{
        PreparedStatement pstm = null;
        Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
        
        String sql = "UPDATE barang SET nama_barang=?, harga_barang=?, jumlah_barang=? WHERE id_barang=?";
        
        try{
            pstm = conn.prepareStatement(sql);
            pstm.setString(4, data.getId_barang());
            pstm.setString(1, data.getNama_barang());
            pstm.setString(2, data.getHarga_barang());
            pstm.setString(3, data.getJumlah_barang());
            pstm.execute();
            return true;
        }
        catch(HeadlessException | SQLException e){
            System.err.println(e);
            return false;
        }
    }
    
     public boolean HapusInfoStokBarang(m_barang data) throws SQLException{
        PreparedStatement pstm = null;
        Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
        
        String sql = "DELETE FROM barang WHERE id_barang=?";
        
        try{
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, data.getId_barang());
            pstm.execute();
            return true;
        }
        catch(HeadlessException | SQLException e){
            System.err.println(e);
            return false;
        }
    }
}