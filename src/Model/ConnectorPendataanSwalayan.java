package Model;
import java.sql.*;
import javax.swing.*;

public class ConnectorPendataanSwalayan {
    private static Connection MySQLConfig;
    public static Connection configDB() throws SQLException{
        try{
            String url = "jdbc:mysql://localhost:3306/pendataan_swalayan"; // url database
            String user = "root";
            String pass = "xxx";
            
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            MySQLConfig = DriverManager.getConnection(url, user, pass);
        }
       catch(SQLException e){
           System.out.println("koneksi gagal "+e.getMessage()); // perintah menampilkan error pada koneksi
        }
        
        return MySQLConfig;
    }
}