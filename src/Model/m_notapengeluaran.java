package Model;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
public class m_notapengeluaran extends ConnectorPendataanSwalayan{  
    private String kode_pengeluaran;
    private String id_barang;
    private String nama_barang;
    private String banyak_barang;
    private String harga_barang;
    private String jumlah_harga;

    public String getKode_pengeluaran() {
        return kode_pengeluaran;
    }

    public void setKode_pengeluaran(String kode_pengeluaran) {
        this.kode_pengeluaran = kode_pengeluaran;
    }

    public String getId_barang() {
        return id_barang;
    }

    public void setId_barang(String id_barang) {
        this.id_barang = id_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getBanyak_barang() {
        return banyak_barang;
    }

    public void setBanyak_barang(String banyak_barang) {
        this.banyak_barang = banyak_barang;
    }

    public String getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(String harga_barang) {
        this.harga_barang = harga_barang;
    }

    public String getJumlah_harga() {
        return jumlah_harga;
    }

    public void setJumlah_harga(String jumlah_harga) {
        this.jumlah_harga = jumlah_harga;
    }
    
     public boolean SimpanNotaPengeluaran(m_notapengeluaran data) throws SQLException{
        PreparedStatement pstm = null;
        Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
        
        String sql = "INSERT INTO nota_pengeluaran(kode_pengeluaran, id_barang, nama_barang, banyak_barang, harga_barang, jumlah_harga) VALUES (?,?,?,?,?,?)";
        
        try{
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, data.getKode_pengeluaran());
            pstm.setString(2, data.getId_barang());
            pstm.setString(3, data.getNama_barang());
            pstm.setString(4, data.getBanyak_barang());
            pstm.setString(5, data.getHarga_barang());
            pstm.setString(6, data.getJumlah_harga());
            pstm.execute();
            
            int jumlah_lama = 0;
            String sql3 = "SELECT jumlah_barang FROM barang WHERE id_barang = '"+getId_barang()+"'";
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql3);

            if(res.next()){
                jumlah_lama = Integer.parseInt(res.getString("jumlah_barang"));
            }
            PreparedStatement pstm1 = null;
            String sql2 = "UPDATE barang SET jumlah_barang = ("+jumlah_lama+"-"+getBanyak_barang()+") WHERE id_barang = '"+getId_barang()+"'";
            pstm1 = conn.prepareStatement(sql2);
            pstm1.execute();
            
            return true;
        }
        catch(HeadlessException | SQLException e){
            System.err.println(e);
            return false;
        }
    }
    
    public boolean UpdateNotaPengeluaran(m_notapengeluaran data) throws SQLException{
        PreparedStatement pstm = null;
        Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
        
        String sql = "UPDATE nota_pengeluaran SET id_barang=?, nama_barang=?, banyak_barang=?, harga_barang=?, jumlah_harga=? WHERE kode_pengeluaran=?";
        
        try{
            pstm = conn.prepareStatement(sql);
            pstm.setString(6, data.getKode_pengeluaran());
            pstm.setString(1, data.getId_barang());
            pstm.setString(2, data.getNama_barang());
            pstm.setString(3, data.getBanyak_barang());
            pstm.setString(4, data.getHarga_barang());
            pstm.setString(5, data.getJumlah_harga());
            pstm.execute();
            return true;
        }
        catch(HeadlessException | SQLException e){
            System.err.println(e);
            return false;
        }
    }
    
     public boolean HapusNotaPengeluaran(m_notapengeluaran data) throws SQLException{
        PreparedStatement pstm = null;
        Connection conn = (Connection)ConnectorPendataanSwalayan.configDB();
        
        String sql = "DELETE FROM nota_pengeluaran WHERE kode_pengeluaran=?";
        
        try{
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, data.getKode_pengeluaran());
            pstm.execute();
            return true;
        }
        catch(HeadlessException | SQLException e){
            System.err.println(e);
            return false;
        }
    }
}
